<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>blackpink - 블랙핑크</title>
</head>
<body>
<?php

$urls = file('urls');
if ($urls) {
    $url = trim($urls[array_rand($urls)]);

    header("Location: $url", false, 307);
?>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="<?php print $url ?>">here</a>.</p>
<?php
} else {
?>
<p>no urls to redirect to</p>
<?php
}

?>
</body>
</html>
